/**
 * @author Rogger
 * @version 1.0
 * @since 07/03/2019
 */

package proyectoJunitRogger;

import java.util.Scanner;

public class JunitLibrerias {

	static Scanner reader = new Scanner(System.in);
	
	static int llibres = 0;
	static int estanterias = 0;

	public static void main(String[] args) {
		
		/**
		 * Crear variables para guardar el n�mero de huecos y el n�mero de libros
		 */
		
		int huecos;
		int num;
		int casos;
		
		System.out.println("Numero de Casos"); 
		casos = reader.nextInt();
		
		for (int i = 0;i<casos;i++) {
			System.out.println("Cuantos huecos quieres que tenga la librer�a?"); 
			huecos = reader.nextInt();
			
			/**
			 * Comprobar que se introduzca un n�mero positivo y que este entre 1 y 1000
			 */
			
			if (huecos <= 0 || huecos > 1000) {
				System.out.println("Tiene que ser un n�mero positivo entre 1 y 1000");
			} else {
				System.out.println("Introduce la cantidad de libros: ");
				num = reader.nextInt();
	
				if (num <= 0 || num > 500) {
					System.out.println("Tienes que introducir un n�mero positivo entre 1 y 500.");
					
				} else {
					librerias(huecos, num);
					estanterias = 0; //Reiniciar la variable "estanterias" porque si no se suma al anterior resultado si hay mas de 1 caso
					llibres = 0;
				}
			}
		}
	}
	
	/**
	 * Bucle para ir rellenando los huecos de las estanter�as con libros y decirnos los que sobran.
	 * @param huecos Espacios en las librer�as para los libros
	 * @param num N�mero total de libros para guardar en las estanter�as
	 */

	public static void librerias(int huecos, int num) {

		for (int i = 0; i < num; i++) {

			llibres++;

			if (llibres == huecos) {
				estanterias++;
				llibres = 0;
			}
		}
		System.out.println("Has llenado: " + estanterias + " estanter�as");
		System.out.println("Te han sobrado un total de:  " + llibres + " libros.");
		System.out.println("\n");
	}

}