package proyectoJunitRogger;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class JunitLibreriasTest {
	int[] huecos = { 25, 30, 50, 60, 20, 100 };
	int[] libros = { 25, 50, 75, 100, 125, 450 };
	int[] relleno = { 1, 2, 3, 5, 5, 5 };
	int[] sobran = { 0, 20, 45, 25, 150, 600 };

	@Test
	void testLibreria() {

		for (int i = 0; i <huecos.length; i++) {
			JunitLibrerias.librerias(huecos[i], libros[i]);
			assertEquals(sobran[i], JunitLibrerias.llibres);
			assertEquals(relleno[i], JunitLibrerias.estanterias);

		}
	}
}